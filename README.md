Sistema de Ponto Eletrõnico - Solides

O arquivo se trata de um sistema de registro de ponto, composto de validação via login.

Compartilhei com o Frederico a versao com CodeIgniter e sem CodeIginiter.

Para executar o sistema, basta:

Baixar o repositório "login-php-codeigniter"

Adicioná-lo a pasta www, dentro da instalação do WAMP.

Pronto! Basta acessar: localhost/login-php-codeigniter/index

Obs: O banco de dados anexo já contém um login:

usuário: glaycon senha:123

Considerações sobre o projeto:

Utilizando o Codeigniter, encontrei problemas na finalização do projeto.

O sistema hospedado é a verão sem MVC do projeto.

O sistema foi hospedado através do hostingers, sob o domínio: http://pontodoestagiario.tech/

Os reposítórios estão disponíveis no GIT HUB através dos links:

Sem MVC: https://github.com/Glaycongx/Solides.git

Com MVC: https://github.com/Glaycongx/login-php-codeigniter.git